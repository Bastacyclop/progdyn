use std::{io, str, fs, env};

fn main() {
    use std::io::BufRead;

    let mut args = env::args().skip(1);
    let path = args.next().unwrap();
    let file = fs::File::open(path).unwrap();
    let mut line = String::new();
    let mut reader = io::BufReader::new(file);

    reader.read_line(&mut line).unwrap();
    let n: usize = line.trim().parse().expect("cannot parse 'n'");
    println!("n: {}", n);
    line.clear();

    let mut sequence = Vec::with_capacity(n);
    while reader.read_line(&mut line).unwrap() > 0 {
        sequence.push(line.trim().parse::<u32>().expect("cannot parse input"));
        line.clear();
    }

    // sequence length -> sequence end value
    let mut best_so_far = Vec::with_capacity(n);
    best_so_far.push(sequence[0]);
    for &a in sequence.iter().skip(1) {
        match best_so_far.binary_search(&a) {
            Result::Err(index) => if index == best_so_far.len() {
                best_so_far.push(a);
            } else {
                best_so_far[index] = a;
            },
            Result::Ok(_) => {}
        }
    }

    println!("best: {}", best_so_far.len());
}