use std::{io, str, fs, env};

fn main() {
    use std::io::BufRead;

    let mut args = env::args().skip(1);
    let path = args.next().unwrap();
    let file = fs::File::open(path).unwrap();
    let mut line = String::new();
    let mut reader = io::BufReader::new(file);

    reader.read_line(&mut line).unwrap();
    let n;
    let c;
    {
        let mut split = line.split_whitespace();
        n = split.next().unwrap().parse::<usize>().expect("cannot parse 'n'");
        println!("n: {}", n);
        c = split.next().unwrap().parse::<usize>().expect("cannot parse 'c'");
        println!("c: {}", c);
    }
    line.clear();

    let mut coins = Vec::with_capacity(n);
    while reader.read_line(&mut line).unwrap() > 0 {
        coins.push(line.trim().parse::<i32>().expect("cannot parse input"));
        line.clear();
    }

    assert!(coins[0] == 1);

    let mut best_so_far = Vec::with_capacity(c + 1);
    best_so_far.push(0u32);
    for _ in 0..c {
        let mut min = best_so_far.last().unwrap() + 1;
        for &coin in coins.iter().skip(1) {
            let index = best_so_far.len() as i32 - coin;
            if index >= 0 {
                let n = best_so_far[index as usize] + 1;
                if n < min { min = n; }
            }
        }
        best_so_far.push(min);
    }

    println!("best: {}", best_so_far.last().unwrap());
}