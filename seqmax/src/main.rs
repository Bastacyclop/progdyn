use std::{io, str, fs, env};

type Size = u32;

fn main() {
    use std::io::BufRead;

    let mut args = env::args().skip(1);
    let path = args.next().unwrap();
    let file = fs::File::open(path).unwrap();
    let mut line = String::new();
    let mut reader = io::BufReader::new(file);

    reader.read_line(&mut line).unwrap();
    let n: Size = line.trim().parse().expect("cannot parse 'n'");
    println!("n: {}", n);
    line.clear();

    let mut entries = Vec::with_capacity(n as usize);

    while reader.read_line(&mut line).unwrap() > 0 {
        entries.push(line.trim().parse().expect("cannot parse input"));
        line.clear();
    }

    let mut max_sum = 0;
    let mut sum = 0;
    for i in 0..entries.len() {
        let a = entries[i];
        sum += a;
        if sum < a {
            sum = a;
        }
        if sum > max_sum {
            max_sum = sum;
        }
    }

    println!("seqmax: {}", max_sum);
}