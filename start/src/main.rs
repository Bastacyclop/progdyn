use std::{io, str, fs, env};

fn main() {
    use std::io::BufRead;

    let mut args = env::args().skip(1);
    let path = args.next().unwrap();
    let file = fs::File::open(path).unwrap();
    let mut line = String::new();
    let mut reader = io::BufReader::new(file);

    reader.read_line(&mut line).unwrap();
    print!("n: {}", line);
    line.clear();

    let mut sum: u64 = 0;
    while reader.read_line(&mut line).unwrap() > 0 {
        let a = line.trim().parse().expect("cannot parse input");
        sum = sum.checked_add(a).expect("overflow");
        line.clear();
    }

    println!("sum: {}", sum);
}
